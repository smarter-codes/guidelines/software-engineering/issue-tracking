# Issue Tracking

* [Document Requirements in Issue Tracker](https://gitlab.com/smarter-codes/guidelines/software-engineering/requirements/documenting-requirements)
* [Open Approach](https://gitlab.com/smarter-codes/guidelines/software-engineering/issue-tracking/-/issues/2#note_521626589)
* [Arrange issues with Priority](#1)
* Schedule issues with Milestones
* Review issues on a Board
